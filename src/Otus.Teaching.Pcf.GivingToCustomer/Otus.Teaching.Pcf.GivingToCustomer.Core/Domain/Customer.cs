﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        : BaseEntity
    {
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = String.Empty;

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; } = String.Empty;

        public ICollection<Preference> Preferences { get; set; }

        public ICollection<Guid> PromoCodeIds { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}