﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private IMongoCollection<Customer> _customerCollection;
        private IMongoCollection<Preference> _preferenceCollection;

        public EfDbInitializer(IMongoDatabase dataContext)
        {
            _db = dataContext;
            _customerCollection = _db.GetCollection<Customer>("Customer");
            _preferenceCollection = _db.GetCollection<Preference>("Preference");
        }

        public void InitializeDb()
        {
            if ( _customerCollection.Find(_ => true).Any<Customer>() ||
                _preferenceCollection.Find(_ => true).Any<Preference>() )
                return;

            _preferenceCollection.InsertManyAsync(FakeDataFactory.Preferences);
            _customerCollection.InsertManyAsync(FakeDataFactory.Customers);
        }
    }
}